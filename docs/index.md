# Nguyễn Doãn Cương

<sup>*Backend Developer - BA - Trader*</sup>

<sup>*090.696.1286 - cuongnd86@gmail.com*</sup>

> ## Giới thiệu

- Tôi có trên 10 năm kinh nghiệm & trải qua hầu hết các vị trí công việc trong lĩnh vực phát triển phần mềm.
- Mong muốn làm việc trong môi trường công ty có nhiều dự án với nhiều ngành nghề, lĩnh vực, đa quốc gia để nâng cao và phát triển kiến thức.

> ## Học vấn
  
- Đại Học Thương Mại Hà Nội (MBA) (01/2013-08/2014).
- Đại Học Công Nghiệp Hà Nội (Khoa học máy tính) (2009-2011).
- Một số chứng chỉ phát triển phần mềm, phân tích nghiệp vụ, marketing, sale.

> ## Vị trí cộng việc mong muốn

- Python, BA, Cloud, DevOps.

> ## Thời gian làm việc

- Kinh nghiệm làm việc từ 2009 - Hiện tại trong lĩnh vực công nghệ thông tin.

> ### 10/2022 - Hiện tại

- Công ty: NTQ Solition
- Vị trí công việc: Technical Leader Python
- **Nhiệm vụ chính:**
  - Phân tích / Thiết kế hệ thống
  - Code / Test / Deploy

> ### 10/2021 - 10/2022

- Công ty: CMC Global
- Vị trí công việc: DevOps
- **Nhiệm vụ chính:**
  - Nghiên cứu các giải pháp CI/CD dự án của khách hàng Nhật Bản
  - Thiết kế & triển khai quy trình CI/CD dự án Mobile. Dev / GitHub CI-CD / CHPlay / các dịch vụ Google Cloud.

> ### 10/2019 - 10/2021

- Công ty: System-Gear Việt Nam
- Vị trí công việc: BA, Developer
- **Nhiệm vụ chính:**
  - Làm việc trực tiếp với khách hàng, tư vấn, giải thích về nghiệp vụ.
  - Phân tích nghiệp vụ hệ thống ERP-Odoo (Project, Task, TimeSheets, Plan, CRM).
  - Phỏng vấn, tuyển dụng nhân sự các dự án.
  - Lập trình ứng dụng: Odoo, Python, React-Nativate quản lý điểm danh, thời giam làm việc của nhân viên trên ứng dụng di động.
  - Thiết lập các API mở rộng tương tác Odoo với các hệ thống khác.

> ### 08/2015 – 11/2015

- Công ty: Honda Việt Nam
- Sản phẩm: Phần mềm quản lý và phản ánh lỗi của các nhà cung cấp phụ tùng. Giúp phòng quản lý chất lượng của HONDA kiểm soát đánh giá - nâng cao chất lượng của các nhà cung cấp.
- Vị trí: Phân tích thiết kế.

> ### 09/2014 – 02/2015

- Công ty: Hoàng Sơn Việt Nam
- Vị trí: Quản lý siêu thị, trưởng phòng IT
- **Nhiệm vụ chính:**
  - Thiết kế và triển khai hạ tầng hệ thống thông tin.
  - Giám sát tiến độ xây dựng và hoàn thiện công trình.
  - Phỏng vấn, tuyển dụng và đào tạo nhân viên các bộ phận.
  - Thiết lập quy trình vận hành hệ thống, giám sát và hỗ trợ các phòng ban.
  - Hỗ trợ phòng kinh doanh, lên kế hoạch và kiểm soát đặt hàng.
  - Hỗ trợ phòng hành chính và thực hiện đào tạo kỹ năng cho nhân viên.
  - Hỗ trợ phòng kế toán thực hiện xử lý phần mềm kế toán siêu thị.
  - Hỗ trợ đào tạo bộ phận thu ngân sử dụng phần mềm bán hàng.
  - Lên kế hoạch tài chính, vật tư cung cấp cho các công trình của công ty.

> ### 09/2009 -  09/2014

- Công ty: FPT Software.
- Vị trí công việc: Quản trị dự án, phân tích thiết kế, lập trình.
- **Nhiệm vụ chính:**
- Phỏng vấn và đào tạo nhân viên mới cho dự án.
- Lập kế hoạch công việc nhóm và từng thành viên.
- Thực hiện kiểm tra đánh giá tiến độ và chất lượng công việc trong dự án.
- Hỗ trợ về kỹ thuật, tổ chức và thực hiện đào tạo kỹ năng cho các thành viên.
- Giao tiếp và báo cáo công việc với PM, khách hàng Nhật Bản.
- Phân tích thiết kế hệ thống phần mềm theo yêu cầu của khách hàng Nhật Bản.
- Nghiên cứu giải pháp và lập trình các công cụ giúp nâng cao hiệu quả công việc, tiết kiệm thời gian và giảm thiểu lỗi cho dự án.
- Thực hiện lập trình và kiểm thử các chương trình phần mềm theo yêu cầu của khách hàng Nhật Bản.

> ## Kỹ năng - Kinh nghiệm

- Có kỹ năng tự nghiên cứu, tự học nhanh 1 ngôn ngữ lập trình hay một công nghệ mới.
- Có kinh nghiệm quản lý, đào tạo và phát triển đội nhóm.
- Có kinh nghiệm lập kế hoạch và triển khai dự án phần mềm.
- Có nhiều kinh nghiệm và hiểu biết sâu về quy trình phát triển phần mềm với khách hàng Nhật Bản.
- Có kỹ năng và kinh nghiệm làm BA.

> ## Kỹ năng chuyên môn

- Hiểu biết sâu về lập trình.
- Thành thạo và làm việc tốt với các ngôn ngữ lập trình: Python, C#.NET, PHP.
- Thành thạo và làm việc tốt với các hệ quản trị cơ sở dữ liệu: SQL Server, MySQL, MongoDB.
- Điểm mạnh: Lập trình, tự động hóa các công việc.


> ## Kỹ năng khác

- Marketing (Google, Facebook).
- Trading, Algo Trading.
- Crawler Data, Automation (Python).

> ## Sở thích

- Trading, lập trình bot giao dịch tiền điện tử & Forex, phật giáo, tài chính.

> ## Tham chiếu

- Mr. Phạm Đức Toàn - PMP, MBA
- Email : phamdtoan@gmail.com
